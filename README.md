Инструкция для установки на локалке.
* Js это вторая ветка. В первой  аджаксом прогружал каждый коммент и открывал внутри аккордеона.
Пытался сделать так, потому что пайтоном нельзя в шаблоне написать рекурсивную функцию для отображения. (Внезапно найдены плюсы PHP). Потом все таки решил все связанное с отображением делать джаваскриптом и на фронтенд отдавать данные  в виде JSONа. Фронт енд очень красивый сделать уже не успеваю((
P.S. В корне лежит файл базы данных. В ней настраивается регистрация через ВК, поэтому я его включил в репозиторий. Если без него, то не будет работать вход через ВК.

* git clone -b js "your link from the clone"
* virtualenv -p python3 lit
* cd lit 
* source bin/activate
* pip install -r requirements.txt
* python manage.py migrate
* python manage.py runserver
* localhost:8000 (именно локалхост, а не 127.0.0.1:8000, потому что редирект после входа ВК идет на локалхост!)
* https://drive.google.com/file/d/0Bx5f9K6GrjugSjhmbWlDSExkc2c/view?usp=sharing