from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Message(models.Model):
    author = models.ForeignKey(User,
                               on_delete=models.CASCADE,
                               verbose_name='author')

    text = models.TextField()

    relation = models.ForeignKey('self',
                                 blank=True,
                                 null=True,
                                 verbose_name='relation',
                                 on_delete=models.CASCADE,)

    date = models.DateTimeField('date published',
                                auto_now=False,
                                auto_now_add=True, )

    def __str__(self):
        if len(self.text) > 10:
            return self.text[0:11] + '...'
        else:
            return self.text