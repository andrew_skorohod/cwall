from django.shortcuts import render
from django.http import HttpResponse,Http404
from django.core import serializers
from django.contrib.auth.models import User

from .models import Message
from .forms import MessageForm
# Create your views here.


def index(request):
    form = MessageForm(request.POST or None)
    msgs = Message.objects.filter(relation__isnull = True).order_by('-date')
    #comments = Message.objects.filter(relation__isnull=False).order_by('date')
    context = {
        'msgs':msgs,
        #'comments':comments,
    }
    return render(request,'index.html',context)

def getComment(request):#ajax
    id = request.GET.get('id')
    try:
        comments = Message.objects.filter(relation=id).order_by('-date')
        comments = serializers.serialize("json", comments)
    except:
        comments = []
    return HttpResponse(comments)


def addComment(request):
    id = request.GET.get('id')
    userId = request.GET.get('userId')
    user = User.objects.get(id=userId)
    text = request.GET.get('text')
    if id:
        relation = Message.objects.get(id=id)
        comment = Message(author=user, relation=relation, text=text)
        comment.save()
    else:
        comment = Message(author=user, text=text)
        comment.save()
    return HttpResponse('ok')

